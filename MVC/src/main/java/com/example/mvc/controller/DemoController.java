package com.example.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.mvc.service.ServiceDemo;

@RestController
@RequestMapping("controller")
public class DemoController {
	
	@Autowired
	private ServiceDemo service;
	
	@GetMapping("/display")
	public String display() {
		//System.out.println("Demo Controller");
		
		return "Demo Controller";
	}
	
	@GetMapping("/servicedisplay")
	public String serviceDisplay() {
		
		return service.show();  // return "show from Service"
	}
	
	@GetMapping("/daodisplay")
	public String display_service_dao() {
		
		return service.DAOshow();  // return ""show from DAO""
	}

}
