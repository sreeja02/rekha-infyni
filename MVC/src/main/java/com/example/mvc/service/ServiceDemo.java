package com.example.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.mvc.dao.DemoDAO;

@Service
public class ServiceDemo {

	@Autowired
	private DemoDAO dao;
	
	public String  show() {
		return "show from Service";
	}
	
	public String DAOshow() {
		return dao.DAOShow();  // return "show from DAO";
	}
}
