package com.example.springsecurity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springsecurity.entity.UserEntity;
import com.example.springsecurity.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService user_service;
	
	
	// Encrypt password : https://bcrypt-generator.com/
	@GetMapping("/save")
	public void save(@RequestBody UserEntity user) {
		user_service.save(user);
	}
	
	@GetMapping("/allusers")
	public List<UserEntity> getAllUSers(){
		return user_service.getAllUsers();
	}
	
	@GetMapping("/{username}")
	public UserEntity getByUsername(@PathVariable String  username) {
		return user_service.getByUserName(username);
	}
}
