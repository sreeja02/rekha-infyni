package com.example.springsecurity.service;

import java.util.List;

import com.example.springsecurity.entity.UserEntity;

public interface UserService {
	public void save(UserEntity user);
	public List<UserEntity> getAllUsers();
	public UserEntity getByUserName(String username);
}
