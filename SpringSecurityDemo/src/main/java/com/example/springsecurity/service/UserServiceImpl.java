package com.example.springsecurity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.example.springsecurity.entity.UserEntity;
import com.example.springsecurity.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository user_repo;
	
	@Override
	public void save(UserEntity user) {
		user_repo.save(user);
		
	}

	@Override
	public List<UserEntity> getAllUsers() {
		return user_repo.findAll();
	}

	@Override
	public UserEntity getByUserName(String username) {
		
		return user_repo.findByUsername(username);
	}

}
