package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Employee;
import com.example.demo.service.EmployeeService;

@RestController
@RequestMapping("/emp")
public class EmployeeController {
	
	@Autowired
	EmployeeService empService;
	
	@GetMapping("/display")
	public List<Employee> findAll(){
		
		return empService.findAll();
	}
	
	@GetMapping("/employee/{empID}")			// empID = 2
	public Employee findById(@PathVariable int empID) throws Exception {
		Employee e = empService.findById(empID);   // findByID(2)
		
		if(e == null)
			throw new Exception("Employee with ID " + empID + " details not found");
		
		return e;   // {"name":"Sreeja","salary":30000,"id":2}
	}

}
