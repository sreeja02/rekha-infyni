package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.EmployeeDAO;
import com.example.demo.entity.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	EmployeeDAO empDAO;
	
	public List<Employee> findAll() {
		// TODO Auto-generated method stub
		return empDAO.findAll();
	}

	
	public Employee findById(int id) {			// id = 2
		// TODO Auto-generated method stub
		return empDAO.findById(id);  // {"name":"Sreeja","salary":30000,"id":2}
	}

}
