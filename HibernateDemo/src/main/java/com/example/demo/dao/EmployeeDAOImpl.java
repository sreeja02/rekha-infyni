package com.example.demo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Employee;

import jakarta.persistence.EntityManager;


@Repository
public class EmployeeDAOImpl implements EmployeeDAO{

	@Autowired
	EntityManager entityManager;
	
	
	public List<Employee> findAll() {
		
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Employee> query = currentSession.createQuery("select e from Employee e",Employee.class);
		
		List<Employee> empList = query.getResultList();
				
		
		return empList ;
	}


	
	public Employee findById(int id) {		// id = 2
		Session currentSession = entityManager.unwrap(Session.class);
		
		return currentSession.get(Employee.class, id); // {"name":"Sreeja","salary":30000,"id":2}
	}


}
