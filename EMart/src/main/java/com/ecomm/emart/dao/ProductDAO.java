package com.ecomm.emart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.ecomm.emart.entity.ProductEntity;
//@RepositoryRestResource(path="products")
@Repository
public interface ProductDAO extends JpaRepository<ProductEntity	, Integer> {

}
