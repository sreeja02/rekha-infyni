package com.ecomm.emart.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="product")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ProductEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="product_id")
	private Integer product_id;
	
	@Column(name="product_name")
	private String product_name;
	
	@Column(name="sku")
	private Integer sku;
	
	@Column(name="description")
	private String description;
	
	@Column(name="price")
	private Float price;
	
	@Column(name="quantity")
	private Integer quantity;
	
	@Column(name="categoryid")
	private Integer categoryid;
	
	@ManyToOne	
	@JoinColumn(name="categoryid",insertable = false,updatable = false)
	private ProductCategoryEntity category; 
}
