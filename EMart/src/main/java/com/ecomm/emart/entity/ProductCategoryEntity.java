package com.ecomm.emart.entity;

import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="Category")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ProductCategoryEntity {

	@Id
	@Column(name="category_id")
	private Integer categoryid;
	
	@Column(name="categoryname")
	private String categoryname;
	
	@Column(name="description")
	private String description;
	
	@OneToMany
	@Getter(value = AccessLevel.NONE)
	@Setter(value = AccessLevel.NONE)
	private Set<ProductEntity> products;
}
