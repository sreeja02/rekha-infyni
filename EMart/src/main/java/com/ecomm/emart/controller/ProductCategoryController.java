package com.ecomm.emart.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecomm.emart.entity.ProductCategoryEntity;
import com.ecomm.emart.exceptions.ProductCategoryException;
import com.ecomm.emart.service.ProductCategoryService;

@RestController
@RequestMapping("/categories")
public class ProductCategoryController {

	@Autowired
	private ProductCategoryService categoryService;

	@GetMapping("/save")
	public ResponseEntity<Map<String, Object>> saveCategory(@RequestBody ProductCategoryEntity category) {
		Map<String, Object> m1 = new LinkedHashMap<String, Object>();

		categoryService.save(category);

		m1.put("status", 1);
		m1.put("message", "Category saved successfully");

		return new ResponseEntity<Map<String, Object>>(HttpStatus.CREATED);
	}
	
	

	@GetMapping("/allcategories")
	public List<ProductCategoryEntity> getAllCategories() throws Exception {
		List<ProductCategoryEntity> categoriesList = categoryService.getAllCategories();

		if (categoriesList == null)
			throw new ProductCategoryException("The List is empty, couldn't find category details...");
		return categoriesList;
	}

	@GetMapping("/{id}")
	public ProductCategoryEntity getCategoryById(@PathVariable Integer id) throws ProductCategoryException {
		ProductCategoryEntity category = null;
		category = categoryService.getCategoryById(id);

		if (category == null)
			throw new ProductCategoryException("Category with ID:" + id + " not found in the list...");

		return category;
	}

	@PutMapping("/update")
	public void updateCategory(@RequestBody ProductCategoryEntity category) throws ProductCategoryException {
		ProductCategoryEntity category_search = categoryService.getCategoryById(category.getCategoryid());

		if (category_search != null)
			categoryService.updateCategory(category);
		else
			throw new ProductCategoryException("Category  not found in the list...");

	}

	@DeleteMapping("/delete/{id}")
	public void deleteCategory(@PathVariable Integer id) throws ProductCategoryException {
		ProductCategoryEntity category_search = categoryService.getCategoryById(id);

		if (category_search != null)
			categoryService.deleteById(id);
		else
			throw new ProductCategoryException(
					"Category with ID:" + id + " not found in the list and delete operation can't be done...");

	}
}
