package com.ecomm.emart.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecomm.emart.entity.ProductEntity;
import com.ecomm.emart.service.ProductService;
import com.ecomm.emart.EMartApplication;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService product_service;
	
	private static final Logger logger = LoggerFactory.getLogger(EMartApplication.class);
	
	
	@GetMapping("/save")
	public void save(@RequestBody ProductEntity product) {
		product_service.save(product);
		logger.info("Product under category ID " +product.getCategoryid()+" save successfully ...");
	}
	
	@GetMapping("/allproducts")
	public List<ProductEntity> getAllProducts(){
		return product_service.getAllProducts();
	}
	
	@GetMapping("/{product_id}")
	public ProductEntity getProductById(@PathVariable Integer product_id) {
		ProductEntity product = product_service.getProductById(product_id);
		
		if(product == null)
			logger.error("Product with ID " + product_id + " details not found");
		else 
			logger.info("Product with ID " + product_id + " details  found");
		
		return product;
	}
}
