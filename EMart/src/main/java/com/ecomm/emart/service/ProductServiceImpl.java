package com.ecomm.emart.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecomm.emart.dao.ProductDAO;
import com.ecomm.emart.entity.ProductEntity;

@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductDAO product_dao;
	
	public void save(ProductEntity product) {
		product_dao.save(product);
	}

	
	public List<ProductEntity> getAllProducts() {
		
		return product_dao.findAll();
	}


	
	public ProductEntity getProductById(Integer id) {
		Optional<ProductEntity> op = product_dao.findById(id);
		
		if(op.isPresent())
			return op.get();
		
		return null;
	}

}
