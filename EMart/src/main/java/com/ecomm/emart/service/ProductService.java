package com.ecomm.emart.service;

import java.util.List;

import com.ecomm.emart.entity.ProductEntity;

public interface ProductService {
	
	public void save(ProductEntity product);
	
	public List<ProductEntity> getAllProducts();

	public ProductEntity getProductById(Integer id);
	
	
	
	
	
	
	

}
