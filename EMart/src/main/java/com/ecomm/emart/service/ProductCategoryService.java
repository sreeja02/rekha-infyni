package com.ecomm.emart.service;

import java.util.List;

import com.ecomm.emart.entity.ProductCategoryEntity;

public interface ProductCategoryService {
	
	public void save(ProductCategoryEntity category);
	public List<ProductCategoryEntity> getAllCategories();
	public ProductCategoryEntity getCategoryById(int id);
	public void updateCategory(ProductCategoryEntity category);
	public void deleteById(Integer id);
}
