package com.ecomm.emart.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ecomm.emart.dao.ProductCategoryDAO;
import com.ecomm.emart.entity.ProductCategoryEntity;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {

	@Autowired
	private ProductCategoryDAO categoryDAO;
	
	public void save(ProductCategoryEntity category) {
		categoryDAO.save(category);
		
	}

	public List<ProductCategoryEntity> getAllCategories() {
		return categoryDAO.findAll();
	}
	
	public ProductCategoryEntity getCategoryById(int id) {
		ProductCategoryEntity category = null;
		
		Optional<ProductCategoryEntity> o = categoryDAO.findById(id);
		if(o.isPresent())
			category = o.get();
		
		return category;
	}


	
	public void updateCategory(ProductCategoryEntity category) {
		categoryDAO.save(category);
		
	}

	
	public void deleteById(Integer id) {
		categoryDAO.deleteById(id);
		
	}

}
