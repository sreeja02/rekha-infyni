package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Movable {

	@Autowired
	@Qualifier("car")
	Directions direction;		// new Car() OR new Truck()
	
	
	
	
	public void display() {
		direction.moveLeft();  // car.moveLeft()
		direction.moveRight(); // car.moveRight()
	}
}
