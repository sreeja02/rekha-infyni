package com.example.demo;

import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.stereotype.Component;

@Component
@Qualifier
public class Car implements Directions {

	
	public void moveLeft() {
		System.out.println("Car moving towards Left...");
		
	}

	
	public void moveRight() {
		System.out.println("Car moving towards Right...");
		
	}

}
