package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DependencyInjectionDemoApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext container = SpringApplication.run(DependencyInjectionDemoApplication.class, args);
		
		
		//Car c = new Car();
		
	/*	Car c = container.getBean(Car.class); //Injects Object from Spring Container
		c.moveLeft();
		c.moveRight();
		
		
		Truck t = container.getBean(Truck.class) ;
		t.moveLeft();
		t.moveRight();*/
		
		
		Movable m = container.getBean(Movable.class);
		//m.direction = container.getBean(Car.class);
		m.display();
	}

}
