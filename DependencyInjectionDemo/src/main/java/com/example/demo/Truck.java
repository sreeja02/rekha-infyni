package com.example.demo;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class Truck implements Directions{

	
	public void moveLeft() {
		System.out.println("Truck moves towards left...");
		
	}

	
	public void moveRight() {
		System.out.println("Truck moves towards right...");
		
	}

}
